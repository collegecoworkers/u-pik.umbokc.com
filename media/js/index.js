$(function(){

	function dbg(mes){
		window.a = mes;
		console.log(mes);
	}

	if (!Array.prototype.last){
		Array.prototype.last = function(){
			if(this.length == 0)
				return undefined;
			return this[this.length - 1];
		};
	};

	var chat_list =  $('.chat-area .chat-list');
	chat_list.jScrollPane({mouseWheelSpeed: 30 });
	var chat_list_api = chat_list.data('jsp');
	var chat_id = location.pathname.replace('/chat/', '');
	var input = $( 'input[input-message]');
	var current_user = $( '[current-user]').attr('current-user');
	var current_conv = $('.conversation-list > ul > li.active');
	var current_conv_id = current_conv.attr('conv-id');
	var messages = [];
	var conversations = {};
	var conversations_list = $( '.conversation-list ul');
	var message_audio = new Audio('/media/audio/vk_notification.mp3');
	var online_audio = new Audio('/media/audio/online.wav');


	chat_list_api.scrollToBottom();

	$('[conv-id]').each(function(index, el) {
		conversations[el.getAttribute('conv-id')] = el.getAttribute('last-mess-id');
	});

	$('.chat-area .chat-list ul li[mess-id]').each(function(index, el) {
		messages.push(el.getAttribute('mess-id'));
	});

	$('.add-message').submit(send_message);
	$('.send-btn').click(send_message);

	$('.expand').click(function(){
		chat_list_api.reinitialise();
		// chat_list_api.scrollToBottom();
	});

	$('.wrap-tab-link').click(function(event) {
		$('.wrap-tab-link').removeClass('active');
		$(this).addClass('active');
	});

	function isEmpty( el ){
		return !$.trim(el.html())
	}

	function hostReachable() {

		// // Handle IE and more capable browsers
		// var xhr = new ( window.ActiveXObject || XMLHttpRequest )( "Microsoft.XMLHTTP" );
		// var status;

		// // Open new request as a HEAD to the root hostname with a random param to bust the cache
		// xhr.open( "HEAD", "//" + window.location.hostname + "/?rand=" + Math.floor((1 + Math.random()) * 0x10000), false );

		// // Issue request and handle response
		// try {
		// 	xhr.send();
		// 	return ( xhr.status >= 200 && (xhr.status < 300 || xhr.status === 304) );
		// } catch (error) {
		// 	return false;
		// }
		return true;
	}

	function add_message(msg, from){
		is_current = current_user == msg.user_id;
		is_current || message_audio.play();
		the_class = (is_current ? 'me' : 'new');

		chat_list_api.getContentPane().find('ul').append(
			'<li class="'+ the_class +'" mess-id='+msg.id+'> '+ 
				'<div class="name"> '+ 
					'<span class=""> ' + msg.name + ' </span> '+ 
				'</div> '+ 
				'<div class="message"> '+ 
					'<p>' + msg.message + '</p> '+ 
					'<span class="msg-time">' + msg.time + '</span> '+  
				'</div> '+ 
			'</li>'
		);
		chat_list_api.reinitialise();
		chat_list_api.scrollToBottom();
	}

	function add_conversation(conv, from){
		user_id = '';
		name = conv.name;
		fa_class = 'fa-user';

		if(conv.type == 1){
			user_id = 'conv-user-id=' + conv.user_id;
			fa_class = 'fa-circle-o status';
		}

		conversations_list.append(
			'<li class="item " '+user_id+' conv-id='+conv.id+'>' +
				'<a href="/chat/'+conv.id+'">' +
					'<i class="fa '+fa_class+'"></i>' +
					'<span>'+name+' </span>' +
					'<i class="fa fa-times"></i>' +
				'</a>' +
			'</li>'
		);
	}

	function update_messagess (){

		if(!hostReachable()){ dbg('No connection to host'); return; }

		last_id = messages.last();
		if(last_id == undefined){ last_id = 0; }

		$.ajax({
			url: location.origin + '/get',
			method: "POST",
			data: { chat : chat_id, last : last_id },
			dataType: "json",
			// async: false,
			success : function( msgs ) {
				if(msgs !== false){
					newmsgs = [];
					for (var i = 0; i < msgs.length; i++) {
						if(!messages.includes(msgs[i].id)){
							messages.push(msgs[i].id);
							newmsgs.push(msgs[i]);
						}
					}
					for (var i = 0; i < newmsgs.length; i++){
						add_message(newmsgs[i], 'update_messagess');
					}
					if(newmsgs.length > 0){
						current_conv.attr('last-mess-id', newmsgs[newmsgs.length-1].id);
						checking_new_messages();
					}
				}
			}
		}).fail(function( jqXHR, textStatus ) {
			dbg("Request failed :#update_messagess: " + textStatus + ' : ' + jqXHR.responseText);
		});
	}

	function to_showed(elem){
		el = $(elem);
		el.removeClass('new');
		url = location.origin + '/showed/' + el.attr('mess-id');
		$.ajax({
			url: url,
			method: "GET",
		}).fail(function( jqXHR, textStatus ) {
			dbg("Request failed :#update_messagess: " + textStatus + ' : ' + jqXHR.responseText);
		});
	}

	function checking_new_messages (){
		if(!hostReachable()){ dbg('No connection to host'); return; }

		$('.chat-area ul > li.new').each(function(index, el) {
			// dbg($(el).attr('mess-id'));
			setTimeout(to_showed.bind(null, el), 5000);
		});
	}

	function checking_online (){

		if(!hostReachable()){ dbg('No connection to host'); return; }

		$.ajax({
			url: location.origin + '/online',
			method: "POST",
			data: {},
			dataType: "json",
			// async: false,
			success : function( users ) {
				if(users !== false){
					for (key in users) {
						if (users.hasOwnProperty(key)) {
							status = users[key].status;
							item2 = $('[conv-user-id='+key+']');
							item2_status = item2.find('.status');

							if((item2_status.hasClass('offline') || item2_status.hasClass('idle')) && status == 'online')
								online_audio.play();

							item2_status.removeClass('online offline idle').addClass(status);

							item = $('[member-user-id='+key+']');
							item.find('.status').removeClass('online offline idle').addClass(status);
							item.find('.time').text(users[key].time);
						}
					}
				}
			}
		}).fail(function( jqXHR, textStatus ) {
			dbg("Request failed :#checking_online: " + textStatus + ' : ' + jqXHR.responseText);
		});
	}

	function checking_convs (){

		if(!hostReachable()){ dbg('No connection to host'); return; }

		$.ajax({
			url: location.origin + '/conv',
			method: "POST",
			data: { convs: conversations, current: current_conv_id },
			dataType: "json",
			// async: false,
			success : function( convs ) {
				// dbg(convs);
				if(convs['new_convs'] !== false){
					newconvs = [];
					for (var i = 0; i < convs.length; i++) {
						if(!conversations.includes(convs[i].id)){
							conversations.push(convs[i].id);
							newconvs.push(convs[i]);
						}
					}
					for (var i = 0; i < newconvs.length; i++){
						add_conversation(newconvs[i], 'checking_convs');
					}
				} else if(convs['new_messages'] !== false){
					new_messages = convs['new_messages'];
					for (key in new_messages) {
						if (new_messages.hasOwnProperty(key)) {
							item = $('[conv-id='+key+'] span .new-mess');
							text = item.text();
							new_text = '+' + new_messages[key];
							if(text != new_text){
								item.text(new_text);
								message_audio.play();
							}
						}
					}
				}
			}
		}).fail(function( jqXHR, textStatus ) {
			dbg("Request failed :#checking_convs: " + textStatus + ' : ' + jqXHR.responseText);
		});
	}

	function send_message(event) {
		event.preventDefault();

		if(!hostReachable()){ dbg('No connection to host'); return; }

		var message = $.trim(input.val());
		if(message == '') return;

		$.ajax({
			url: location.origin + '/add',
			method: "POST",
			data: { chat : chat_id, message : message },
			dataType: "json",
			// async: false,
		}).done(function( msg ) {
			input.val('');
			update_messagess();
		}).fail(function( jqXHR, textStatus ) {
			sending = false;
			dbg("Request failed: " + textStatus + ' : ' + jqXHR.responseText);
		});

		return false;
	}

	function requests(){
		setInterval(update_messagess, 1000);
		setInterval(checking_online, 20000);
		setInterval(checking_convs, 3000);
	}

	checking_new_messages();

	requests();

});