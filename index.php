<?php
// Umbokc
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 'on');
error_reporting(E_ALL);

define('BASE_DIR', getcwd());

define('APP_DIR', BASE_DIR . '/app');
define('MEDIA_DIR', BASE_DIR . '/media');
define('STATIC_DIR', BASE_DIR . '/static');

define('CONFIG_DIR', APP_DIR . '/config');
define('VIEWS_DIR', APP_DIR . '/views');
define('FRAMEWORK_DIR', APP_DIR . '/frameworks');


try {

	// include all
	require APP_DIR . '/start.php';

	R::ext('xdispense', function( $type ){ 
		return R::getRedBean()->dispense( $type ); 
	});

	app\App::init();

	app\App::initDB();
	$router = new Router();
	$router->run();
	app\App::closeDB();

} catch (Exception $e) {
	echo "<pre>";
	echo ($e);
	echo "</pre>";
}
