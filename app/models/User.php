<?php 
namespace app\models;
use app\modules\F;
/**
* User
*/
class User extends Model {

	protected static $table = 'users';

	public function login($params){
		$user = self::getByField('username', $params->username);

		if ($user->is_isset()){
			foreach ($user as $key => $value) $this->{$key} = $value;
			return $this->validate_password($params->password);
		}

		return false;
	}

	public function create($params){
		$data = [];

		foreach ($params as $key => $value) {
			if($key == 'password')
				$value = self::password_encrypt($value);
			$data[$key] = $value;
		}

		$data['registered'] = F::getDate();
		$data['picture'] = '/media/users/pictures/default.jpg';
		$data['ip'] = F::getIp();

		$this->new($data);

		return true;
	}

	public static function generate_salt($length){
		// Random text
		$unique_random_string = md5(uniqid(mt_rand(), true));

		// Valid characters for a salt are [a-zA-Z0-9./]
		$base64_string = base64_encode($unique_random_string);

		// But not '+' which is valid in base64 encoding
		$modified_base64_string = str_replace('+', '.', $base64_string);

		// Truncate string to the correct length
		$salt = substr($modified_base64_string, 0, $length);
		return $salt;
	}

	public static function password_encrypt($password){
		$hash_format = '$2y$10$';
		$salt_length = 22;			// Blowfish salts should be 22-cheracters
		$salt = self::generate_salt($salt_length);
		$format_and_salt = $hash_format . $salt;
		$hash = crypt($password, $format_and_salt);
		return $hash;
	}

	public function validate_password($password) {
		return crypt($password, $this->password) === $this->password;
	}

}