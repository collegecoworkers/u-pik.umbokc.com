<?php 
namespace app\models;
use app\modules\F;
/**
* Conversations
*/
class Conversations extends Model {

	protected static $table = 'conversations';

	public function getMessages($lim = 30){
		// return Messages::getAllBy('conversation_id', $this->id);
		return Messages::getAllByAndLast('conversation_id', $this->id, $lim);
	}

	public static function ofUser($user_id){
		// $conv_ids = UserConversations::findCol('conversation_id', 'user_id = ?', [$user_id]);
		$conv_ids = UserConversations::find('user_id = ?', [$user_id]);

		if(empty($conv_ids))
			return [];

		$conv_ids = F::map($conv_ids, 'conversation_id');
		$convs = self::getIn('id', $conv_ids);
		$res_convs = [];
		foreach ($convs as $key => $item) {
			$res_convs[$key] = $item->to_arr();
			if($item->type == 1){
				$companion = UserConversations::findCol('user_id', 'conversation_id = ? and user_id <> ?', [$item->id, $user_id]);
				$user = User::findOne('id = ?', [$companion->{'0'}]);
				$res_convs[$key]['name'] = $user->fullname;
				$res_convs[$key]['user_id'] = $user->id;
			}
		}
		return $res_convs;
	}

}