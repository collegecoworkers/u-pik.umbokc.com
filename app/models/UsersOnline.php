<?php 
namespace app\models;
use app\modules\F;
/**
* UsersOnline
*/

class UsersOnline extends Model {

	protected static $table = 'users_online';

	public function offline(){
		$this->remove();
		return true;
	}

}