<?php 
/**
* SiteController
*/

use app\App;
use app\modules\RenderVars;
use app\modules\F;

use app\models\User;
use app\models\UsersOnline;
use app\models\Conversations;
use app\models\UserConversations;
use app\models\Messages;

class SiteController extends Controller {

	function beforeAction() {
		if(App::$user->isGuest)
			return $this->redirect('/signin');

		RenderVars::set('curr_user', App::$user->data);

		$this->checkingOnline();
	}

	function actionIndex() {
		return $this->redirect('/chat/1');
	}

	function actionChat($id) {

		$conv = Conversations::getById($id);
		$conversations = Conversations::ofUser(App::$user->id);

		if(!F::hasVals($conversations, ['user_id' => App::$user->id, 'id' => $conv->id])){
			return F::drop_404();
		}

		$links = [];
		$messages = $conv->getMessages(50);
		foreach ($messages as $key => $item) {
			$messages[$key]->time = F::formatDate($item->time);
			$ls = F::get_links($item->message);
			if(!empty($ls)) $links[] = $ls;
			$messages[$key]->message = F::check_url($item->message);
		}

		$users = [];
		$users_ = User::getAll();
		foreach ($users_ as $item) {
			$users[$item->id] = $item->to_arr();
		}

		return $this->render('site/index', [
			'title' => 'Чат',
			'users' => $users,
			'users_online' => $this->users_online(),
			'messages' => $messages,
			'cur_conv' => $conv->id,
			'conversations' => $conversations,
			'links' => $links,
		]);
	}

	function actionCreate($user_id) {
		$conv_curr = UserConversations::query('SELECT DISTINCT `conversation_id` FROM', 'user_id = ?', [App::$user->id]);
		$conv_with = UserConversations::query('SELECT DISTINCT `conversation_id` FROM', 'user_id = ?', [$user_id]);

		function call_to_arr(&$item1, $key){ $item1 = $item1->to_arr(); }
		array_walk($conv_curr, 'call_to_arr');
		array_walk($conv_with, 'call_to_arr');

		function serialize_array_values($arr){
			foreach($arr as $key=>$val){
				sort($val);
				$arr[$key]=serialize($val);
			}
			return $arr;
		}

		$intersect = array_map("unserialize", array_intersect(serialize_array_values($conv_curr),serialize_array_values($conv_with)));

		foreach ($intersect as $item) {
			$id = $item[0];
			$conv = Conversations::findOne('id = ? and type = 1', [$id]);
			if($conv->is_isset()){
				return $this->redirect('/chat/'.$conv->id);
			}
		}

		$new_conv = new Conversations();
		$new_conv->new(['type' => 1]);
		$new_conv_id = $new_conv->save();

		$conv_curr = new UserConversations();
		$conv_curr->new(['conversation_id' => $new_conv_id, 'user_id' => App::$user->id,]);
		$conv_curr->save();

		$conv_with = new UserConversations();
		$conv_with->new(['conversation_id' => $new_conv_id, 'user_id' => $user_id,]);
		$conv_with->save();
		return $this->redirect('/chat/' . $new_conv_id);
	}

	function actionGet(){

		if (!App::post() or !App::post()->has('last') or !App::post()->has('chat')) {echo false; return false;}

		$conv = $this->check_conv(App::$user->id, App::post()->chat);

		if($conv === false) {echo false; return false;}

		$data = [];

		$last_id = App::post()->last;
		if($last_id > 0)
			$mess = Messages::find('id > ? and conversation_id = ?', [$last_id-1, $conv->id]);
		else
			$mess = Messages::find('conversation_id = ?', [$conv->id]);

		if(empty($mess) or end($mess)->id == $last_id){
			$data = false;
		} else {
			foreach ($mess as $item) {
				$user_fullname = User::findColById($item->user_id, 'fullname')->get(0);
				$data[] = [
					'id' => $item->id,
					'user_id' => $item->user_id,
					'name' => $user_fullname,
					'message' => F::check_url($item->message),
					'time' => F::formatDate($item->time),
				];
			}
		}

		echo json_encode($data);
		return true;
	}

	function actionAdd(){

		if (!App::post() or !App::post()->has('message') or !App::post()->has('chat')) {echo false; return false;}

		$conv = $this->check_conv(App::$user->id, App::post()->chat);

		if($conv === false) {echo false; return false;}

		$data = [];

		$mess = new Messages();

		$mess->new([
			'conversation_id' => $conv->id,
			'user_id' => App::$user->id,
			'message' => App::post()->message,
			'time' => F::getDate(),
		]);

		$mess_id = $mess->save();

		$data = [
			'id' => $mess_id,
			'user_id' => $mess->user_id,
			'name' => App::$user->fullname,
			'message' => $mess->message,
			'time' => F::formatDate($mess->time),
		];


		$conv->set('last_mess', $mess_id);
		$conv->save();

		echo json_encode($data);
		return true;
	}

	function actionOnline(){
		echo json_encode($this->users_online());
		return true;
	}

	function actionConv(){

		if (!App::post() or !App::post()->has('convs') or !App::post()->has('current')) {echo false; return false;}
		$convs_ids = App::post()->convs;
		$user_convs = Conversations::ofUser(App::$user->id);

		$new_convs = [];
		$new_messages = [];

		foreach ($user_convs as $conv) {
			if(!array_key_exists($conv['id'], $convs_ids)){
				if($conv['type'] == 1){
					$new_convs[] = [
						'id' => $conv['id'],
						'name' => $conv['name'],
						'last_mess' => $conv['last_mess'],
						'type' => 1,
						'user_id' => $conv['user_id'],
					];
				} else {
					$new_convs[] = [
						'id' => $conv['id'],
						'name' => $conv['name'],
						'last_mess' => $conv['last_mess'],
						'type' => 0,
					];
				}
			}
		}

		foreach ($convs_ids as $id => $id_mess) {
			if($id != App::post()->current){
				$the_last_mess = Messages::find('conversation_id = ? and user_id <> ? and seen = 0 ORDER BY `id` DESC', [$id, App::$user->id]);
				if(!empty($the_last_mess))
					$new_messages[$id] = count($the_last_mess);
			}
		}

		echo json_encode([
			'new_convs' => empty($new_convs) ? false : $new_convs,
			'new_messages' => empty($new_messages) ? false : $new_messages,
		]);
		return false;
	}

	function actionShowed($id){
		$mess = Messages::getById($id);
		if($mess->is_isset()){
			$mess->set('seen', 1);
			$mess->save();
		}

		return false;
	}

	function actionUpdate(){
		// $convs = Conversations::getAll();
		// function call_to_arr(&$item, $key){
		// 	$message = Messages::query('SELECT id FROM', 'conversation_id = ? ORDER BY id DESC LIMIT 1', [$item->id]);
		// 	$id = empty($message) ? null : $message[0]->id ;
		// 	$item->set('last_mess', $id);
		// 	$item->save();
		// }

		// array_walk($convs, 'call_to_arr');

		// F::dbg($convs);


		// foreach (Messages::getAll() as $item) {
		// 	$item->set('seen', 1);
		// 	$item->save();
		// }

		return false;
	}


	function checkingOnline(){

		$now = date_create(F::getDate());
		$last = date_create(App::$session->last_seen);
		$time = F::formatDate(F::formatAllDate($last));
		$diff = date_diff($last, $now);

		if(F::getSeconds($diff) >= 20){
			$time = F::formatAllDate($now);
			App::$session->set()['last_seen'] = $time;
			$user_online = UsersOnline::getByField('user_id', [App::$user->id]);
			if ($user_online->is_isset()){
				$user_online->set('last_seen', $time);
			} else {
				$data = ['user_id' => App::$user->id, 'last_seen' => $time];
				$user_online->new($data);
			}
			$user_online->save();

			$time = F::formatDate($time);
		}

		return $time;
	}

	function users_online(){
		$data = [];
		$users_online = F::map(UsersOnline::getAll(), 'user_id', 'last_seen');
		foreach ($users_online as $key => $value) {
			$data[$key] = [
				'time' => F::formatDate($value),
				'status' => $this->is_offline($value),
			];
		}
		return $data;
	}

	function is_offline($last_date){

		$now = date_create(F::getDate());
		$last = date_create($last_date);
		$diff = date_diff($last, $now);
		$diff_sec = F::getSeconds($diff);

		if($diff_sec >= 300){
			return 'offline';
		} else if ($diff_sec >= 60){
			return 'idle';
		} else {
			return 'online';
		}
	}

	function check_conv($user_id, $conv_id){
		$uc = UserConversations::findOne('user_id = ? and conversation_id = ?', [$user_id, $conv_id]);
		if(!$uc->is_isset()) return false;

		$conv = Conversations::findOne('id = ?', [$uc->conversation_id]);
		if(!$conv->is_isset()) return false;

		return $conv;
	}

}