<?php 
/**
* AuthController
*/

use app\App;
use app\modules\RenderVars;
use app\modules\F;

use app\models\User;
use app\models\UsersOnline;
use app\models\UserConversations;

class AuthController extends Controller {

	function actionSignin() {
		if(!App::$user->isGuest)
			return $this->redirect('/');

		if(App::post()){
			$user = new User();
			if($user->login(App::post()) and App::$user->login($user)){

				$time = F::getDate();
				App::$session->set()['last_seen'] = $time;

				$user_online = UsersOnline::getByField('user_id', [App::$user->id]);

				if ($user_online->is_isset()){
					$user_online->set('last_seen', $time);
				} else {
					$data = ['user_id' => App::$user->id, 'last_seen' => $time];
					$user_online->new($data);
				}

				$user_online->save();

				return $this->redirect('/');
			}
		}

		return $this->render('auth/signin', [
			'title' => 'Авторизация'
		]);

		return true;
	}

	function actionSignup() {
		if(!App::$user->isGuest)
			return $this->redirect('/');

		if(App::post()){
			$user = new User();
			if($user->create(App::post()->to_arr()) and $user_id = $user->save()){
				$conv_curr = new UserConversations();
				$conv_curr->new(['conversation_id' => 1, 'user_id' => $user_id,]);
				$conv_curr->save();
				return $this->redirect('/signin');
			}
			F::dbg('error',1);
		}

		return $this->render('auth/signup', [
			'title' => 'Регистрация'
		]);
	}

	function actionLogout() {
		if(!App::$user->isGuest){

			$time = F::getDate();

			$user_online = UsersOnline::getByField('user_id', [App::$user->id]);

			if ($user_online->is_isset()){
				$user_online->set('last_seen', $time);
				$user_online->save();
			}

			App::$user->logout();
			return $this->redirect('/signin');
		}
		return $this->redirect('/');
	}
}