<?php
return array(

	'^/*$' => 'site/index',
	'^\?\w\=.+' => 'site/index',
	'^index$' => 'site/index',

	'^get$' => 'site/get',
	'^add$' => 'site/add',
	'^online$' => 'site/online',
	'^conv$' => 'site/conv',
	'^update$' => 'site/update',

	'^showed/(\d+)$' => 'site/showed/$1',

	'^create/(\d+)$' => 'site/create/$1',
	'^chat/(\d+)$' => 'site/chat/$1',

	'^signin$' => 'auth/signin',
	'^signup$' => 'auth/signup',
	'^logout$' => 'auth/logout',

);