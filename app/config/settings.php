<?php

return array(
	'project' => 'Messenger',
	'production' => false,
	'debug' => false,

	'apps' => array(
		'Essentials',
		'404'
	),

	'database' => array(
		'connect' => 'mysql:host=127.0.0.1;dbname=st-umbokc_u-pikalov',
		'username' => '046652434_u-pika',
		'password' => '046652434'
	),

	'mailer' => array(
		'system_from' => 'info@the-uweb.ru',
	),

	'twig' => array(
		'cache_enabled' => false
	)
);