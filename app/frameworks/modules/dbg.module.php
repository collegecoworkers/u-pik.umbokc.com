<?php
namespace app\modules;

function dbg($s, $d = false){
	echo "<pre>";

	if($s)
		print_r($s);
	else
		var_dump($s);

	echo "\n</pre>";

	if($d)
		die;
}

// function dbg($p, $d = false){
// 	if(is_string($p)){

// 		echo $p;

// 	} else {
// 		echo "<pre>";
// 		print_r($p);
// 		echo "</pre>";
// 	}

// 	if($d) die();
// }