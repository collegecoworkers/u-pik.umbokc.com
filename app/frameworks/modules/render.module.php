<?php
namespace app\modules;

/**
 * Render module.
 * Providers template rendering functionality.
 * Twig templating system is integrated.
 */

function render($template, $variables = [], $force_display = true) {

	$path_parts = explode('/', $template);
	foreach( $path_parts as &$pp ) {
		$pp = strtolower($pp);
	}
	$path_parts[] = strtolower(array_pop($path_parts));

	$loader = new \Twig_Loader_Filesystem( VIEWS_DIR );
	if( count($et = explode('/', $template)) > 1 ) {
		
		if( file_exists(VIEWS_DIR . '/' . join('/', array_slice($path_parts, 0, count($path_parts) - 1))) ) {

			for( $i = 0; $i < count($et) - 1; $i++) {
				$loader->addPath( VIEWS_DIR . '/' . strtolower(array_shift($et))) ;
			}
		}
		$template = array_pop($et);
	}

	$loader->addPath( VIEWS_DIR . '/' );

	$twig_render_params = array();
	
	if( \app\App::settings()['twig']['cache_enabled'] ) {
		$twig_render_params['cache'] = TWIG_CACHE_DIR;
	}

	$twig = new \Twig_Environment($loader, $twig_render_params);

	RenderVars::get('app')['controller'] = \app\App::$controller;
	RenderVars::get('app')['action'] = \app\App::$action;

	if( $force_display ) {
		$twig->display(
			$template . '.html',
			array_merge(RenderVars::get(), $variables)
		);
	} else {
		return $twig->render(
			$template . '.html',
			array_merge(RenderVars::get(), $variables)
		);
	}

}
