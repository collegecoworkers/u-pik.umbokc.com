<?php
namespace app\modules;
/**
* Render vars
*/
class RenderVars {

	public static $variables = array();

	public static function init(){
		self::$variables['app']['settings'] = \app\App::settings();
		self::$variables['title'] = self::$variables['app']['settings']['project'];
		self::$variables['app']['host'] = 'http://' . $_SERVER['HTTP_HOST'];
	}
	
	public static function set($vars_or_key, $value = '') {
		if(is_array($vars_or_key)){
			self::$variables = array_merge(self::$variables, $vars_or_key);
		} else {
			self::$variables[$vars_or_key] = $value;
		}
	}

	public static function &get($key = null) {
		if($key === null){
			return self::$variables;
		} else {
			return self::$variables[$key];
		}
	}

}