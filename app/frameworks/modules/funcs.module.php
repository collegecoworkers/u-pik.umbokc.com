<?php
namespace app\modules;

/**
* F
*/
class F {

	private static $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

	public static function getIp(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			return $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

	public static function getDate(){
		return date("Y-m-d H:i:s");
	}

	public static function formatDate($date){
		return date("H:i", strtotime($date));
	}

	public static function check_url($str){
		$urls = array();
		$urlsToReplace = array();
		if(preg_match_all(self::$reg_exUrl, $str, $urls)) {
			$numOfMatches = count($urls[0]);
			$numOfUrlsToReplace = 0;
			for($i=0; $i<$numOfMatches; $i++) {
				$alreadyAdded = false;
				$numOfUrlsToReplace = count($urlsToReplace);
				for($j=0; $j<$numOfUrlsToReplace; $j++) {
					if($urlsToReplace[$j] == $urls[0][$i]) {
						$alreadyAdded = true;
					}
				}
				if(!$alreadyAdded) {
					array_push($urlsToReplace, $urls[0][$i]);
				}
			}
			$numOfUrlsToReplace = count($urlsToReplace);
			for($i=0; $i<$numOfUrlsToReplace; $i++) {
				$str = str_replace($urlsToReplace[$i], "<a href=\"".$urlsToReplace[$i]."\" target=_blank>".$urlsToReplace[$i]."</a> ", $str);
			}
			return $str;
		} else {
			return $str;
		}
	}

	public static function get_links($string){
		preg_match_all(self::$reg_exUrl, $string, $match);
		return $match[0];
	}

	public static function formatAllDate($date){
		if(is_object($date))
			return $date->format("Y-m-d H:i:s");
		else
			return date("Y-m-d H:i:s", strtotime($date));
	}

	public static function getSeconds($diff){
		$diff_d = $diff->d;
		$diff_h = $diff->h + ($diff_d * 24);
		$diff_min = $diff->i + ($diff_h * 60);
		$diff_s = $diff->s + ($diff_min * 60);
		return $diff_s;
	}

	public static function render_url($template, $variables = array()) {
		return array( 'render_url' => array($template, $variables) );
	}

	public static function redirect($url = '/') {
		header('Location: ' . $url);
		exit();
	}

	public static function drop_404() {
		render('404');
		exit();
	}

	public static function dbg($s, $d = true){
		echo "<pre>";

		if($s)
			print_r($s);
		else
			var_dump($s);

		echo "\n</pre>";

		if($d)
			die;
	}

	public static function map($arr, $field, $field2 = null){
		$res = [];

		$f2Null = $field2 == null;
		$key_val = $f2Null ? $field : $field2;

		foreach ($arr as $value) {
			$is_arr = is_array($value);
			$data = $is_arr ? $value[$key_val] : is_object($value) ? $value->{$key_val} : $value ;

			if($f2Null){
				$res[] = $data;
			} else {
				$val_as_key = $is_arr ? $value[$field] : $value->{$field};
				$res[$val_as_key] = $data;
			}

		}

		return $res;
	}

	public static function hasVal($arr, $field, $val){
		$res = false;
		$is_arr = is_array($arr);
		foreach ($arr as $value) {
			if($is_arr){
				if($value->{$field} == $val)
					$res = true;
			} else {
				if($value[$field] == $val)
					$res = true;
			}
		}
		return $res;
	}

	public static function hasVals($arr, $arr2){
		$res = false;
		foreach ($arr as $item) {
			$is_arr = is_array($item);
			$p = 0;
			foreach ($arr2 as $key => $value) {
				if($is_arr){
					$get_val = isset($item[$key]) ? $item[$key] : null;
				} else {
					$get_val = isset($item->{$key}) ? $item->{$key} : null;
				}

				if($get_val == $value){
					$res = true;
					$p++;
				} elseif ($p >= 1) {
					$res = false;
					break;
				}
			}
		}
		return $res;
	}
}
