<?php
/**
* Router
*/
use function app\modules\render;
use function app\modules\dbg;

class Router{
	private $routes = array();

	function __construct() {
		$this->routes = include(CONFIG_DIR . '/routes.php');
	}

	/**
	* Returns request string
	* @return string
	*/
	private function getURI() {
		if(!empty($_SERVER['REQUEST_URI'])) {
			return str_replace('/?' . $_SERVER['QUERY_STRING'], '', trim($_SERVER['REQUEST_URI'], '/'));
		}
	}

	public function run() {
		$uri = $this->getURI();
		foreach ($this->routes as $uriPattern => $path) {
			if(preg_match("|$uriPattern|", $uri)){

				$internalRoute = preg_replace("|$uriPattern|", $path, $uri);
 
				$segments = explode('/', $internalRoute);

				$controllerName = ucfirst(array_shift($segments) . 'Controller');
				$actionName = 'action' . ucfirst(array_shift($segments));

				$params = $segments;

				$controllerFile = APP_DIR . '/controllers/' . $controllerName . '.php';

				if(file_exists($controllerFile)){
					require_once $controllerFile;
				} else {
					break;
				}

				\app\App::$controller = str_replace('Controller', '', $controllerName);
				\app\App::$action = str_replace('action', '', $actionName);

				$controllerObject = new $controllerName;

				if(method_exists($controllerObject, 'beforeAction'))
					$result = call_user_func(array($controllerObject, 'beforeAction'));

				$result = call_user_func_array(array($controllerObject, $actionName), $params);

				if($result !== null){
					break;
				}
			}
		}
		if(\app\App::$controller === null){
			render('404');
			die();
		}
	}
}