<?php

use app\modules\F;
use function app\modules\dbg;
use function app\modules\render;

/**
* Controller
*/
class Controller {
	public function __construct() {
	}

	public function redirect($url) {
		F::redirect($url);
	}

	public function render($path, $vars = [], $force = true) {
		$res = render($path, $vars, $force);
		return $force ? true : $res;
	}
}