<?php

namespace app;
use app\modules\RenderVars;

class App {

	public static $settings = array();
	public static $user;
	public static $post;
	public static $get;
	public static $session;
	public static $controller = '';
	public static $action = '';

	static public function init() {
		// Запуск сессии для хранения временных данных
		session_start();

		self::$settings = require( CONFIG_DIR . '/settings.php' );
		self::$post = new PostData();
		self::$get = new GetData();
		self::$session = new SessionData();
		self::$user = new AppUser();

		RenderVars::init();
	}

	static public function initDB() {
		\R::setup(
			self::$settings['database']['connect'],
			self::$settings['database']['username'], 
			self::$settings['database']['password']
		);
	}

	static public function post() {
		if(self::$post->count() < 1)
			return null;

		return self::$post;
	}

	static public function get() {
		if(self::$get->count() < 1)
			return null;

		return self::$get;
	}

	static public function session() {
		return self::$session;
	}

	static public function closeDB() {
		\R::close();
	}

	static public function &settings() {
		return self::$settings;
	}
}

/**
* PostData
*/
class PostData {
	private $data = [];

	public function __construct() {
		$this->data = $_POST;
	}

	public function count() {
		return count($this->data);
	}

	public function has($index) {
		return array_key_exists($index, $this->data);
	}

	public function __get($index) {
		if($this->has($index))
			return $this->data[$index];
		return null;
	}

	public function __sleep() {
		return $this->data;
		// return get_object_vars($this);
	}

	public function to_arr() {
		return $this->data;
		// return get_object_vars($this);
	}
}

/**
* GetData
*/
class GetData {
	private $data;

	public function __construct() {
		$this->data = $_GET;
	}

	public function count() {
		return count($this->data);
	}

	public function has($index) {
		return array_key_exists($index, $this->data);
	}

	public function __get($index) {
		if($this->has($index))
			return $this->data[$index];
		return null;
	}
}

/**
* SessionData
*/
class SessionData {

	public function __construct() {
		return $this;
	}

	public function count() {
		return count($_SESSION);
	}

	public function has($index) {
		return array_key_exists($index, $_SESSION);
	}

	public function __get($index) {
		return $_SESSION[$index];
	}

	public function &get($index = null) {
		if($index !== null)
			return $this->{$index};
		else
			return $_SESSION;
	}

	public function &set($index = null, $val = null) {
		if($index !== null)
			$_SESSION[$index];
		else
			return $_SESSION;
	}
}

/**
* AppUser
*/
class AppUser {

	public $isGuest = true;
	public $data;

	public function __construct() {
		$this->isGuest = !isset($_SESSION['app']['user']);
		if(!$this->isGuest){
			$this->data = $_SESSION['app']['user']['data'];
		}
	}

	public function login($model) {

		$this->isGuest = false;

		foreach ($model as $key => $value) {
			$this->data[$key] = $value;
		}

		$_SESSION['app']['user']['data'] = $this->data;

		return true;
	}

	public function logout() {

		unset($_SESSION['app']['user']);

		session_destroy();

		return true;
	}

	public function __get($index) {
		return is_array($this->data) ? $this->data[$index] : $this->data->{$index};
	}
}