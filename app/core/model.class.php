<?php
namespace app\models;
use \R;
use app\modules\F;
/**
* Model
*/
abstract class Model {

	protected $model;

	public static function getById($id){
		return self::getBy('id = ?', [$id]);
	}

	public static function findOne($val, $arr = null){
		if(is_string($val))
			return self::getBy($val, $arr);
		else
			return self::getById($val);
	}

	public static function findColById($id, $col){
		return self::findCol($col, 'id = ?', [$id]);
	}

	public static function findCol($col, $cond, $cond_vals){
		$res = R::getCol("SELECT `$col` FROM " . self::get_table_name() . " WHERE $cond", $cond_vals);
		return self::loadR($res);
	}

	public static function getByField($f, $val){
		return self::getBy("`$f` = ?", $val);
	}

	public static function getBy($cond, $val){
		$arr = is_array($val) ? $val : [$val];
		$res = R::findOne(self::get_table_name(), $cond , $arr);
		return self::loadR($res);
	}

	public static function find(){
		$args = func_get_args(); 
		$classR = 'R';
		$method = 'find';
		array_unshift($args, self::get_table_name());
		$res = call_user_func_array(array($classR, $method), $args);

		return self::loadMultR($res);
	}

	public static function getIn($field, $vals){
		return self::find(" `$field` IN (". R::genSlots($vals) .') ', $vals);
	}

	public static function getAll(){
		$res = R::find(self::get_table_name());
		return self::loadMultR($res);
	}

	public static function getAllBy($field, $val){
		$res = R::find(self::get_table_name(), "$field = ?" , array($val));
		return self::loadMultR($res);
	}

	public static function getAllByAndLast($field, $val, $lim){

		$count = R::count( self::get_table_name(), " `$field` = :val ", [':val' => $val] );

		$start = $count - $lim;
		if($start < 0) $start = 0;

		$res = R::find(self::get_table_name(), "`$field` = :val ORDER BY id ASC LIMIT :start, :limit" , [
			':val' => $val,
			':start' => $start,
			':limit' => $lim,
		]);

		return self::loadMultR($res);
	}

	public static function query($select, $cond, $arr){
		$table_name = self::get_table_name();
		$q = " $select `$table_name` WHERE $cond ";
		$res = R::getAll($q , $arr);
		return self::loadMultR($res);
	}

	public static function getAllQ($cond, $by = 'id', $ord = 'desc', $lim = 30){
		$table_name = self::get_table_name();
		$res = R::getAll("SELECT * FROM `$table_name` WHERE :cond ORDER BY  :by :ord LIMIT 0, :lim" , [
			':cond' => $cond,
			':by' => $by,
			':ord' => $ord,
			':lim' => $lim,
		]);

		return self::loadMultR($res);
	}

	public static function loadR($params){
		$cl = self::get_class_name();
		$obj = new $cl();
		$obj->load($params);

		return $obj;
	}

	public static function loadMultR($params){
		$cls = [];
		$cl = self::get_class_name();
		if(!empty($params)){
			foreach ($params as $val) {
				$obj = new $cl();
				$obj->load($val);
				$cls[] = $obj;
			}
		}

		return $cls;
	}

	public function load($params){

		$this->model = $params;
		if(!empty($params)){
			foreach ($params as $key => $value) {
				$this->{$key} = $value;
			}
		} else {
			$this->model = null;
		}
	}

	public function new($params){
		$this->model = R::xdispense( self::get_table_name() );
		if(!empty($params)){
			foreach ($params as $key => $value) {
				$this->model->{$key} = $value;
			}
			$this->load($this->model);
		}
	}

	public function save(){
		if($this->model)
			return R::store( $this->model );
	}

	public function remove(){
		if($this->model)
			return R::trash( $this->model );
	}

	public function get($index){
		if(isset($this->{$index}))
			return $this->{$index};
		if($this->model != null)
			return $this->model->{$index};

		return null;
	}

	public function set($index, $val){
		if($this->model != null){
			$this->model->{$index} = $val;
			$this->load($this->model);
		}
	}

	public function is_isset(){
		if($this->model == null)
			return false;
		return true;
	}

	protected static function get_table_name(){
		if(isset(static::$table) && trim(static::$table) !== '')
			return static::$table;

		return strtolower(get_called_class()) . 's';
	}

	protected static function get_class_name(){
		return '\\'. get_called_class();
	}

	// public function __set($index, $val){
	// 	if(isset($this->{$index})) {
	// 		$this->{$index} = $val;
	// 	} else {
	// 		$arr = $this->to_arr();
	// 		$arr[$index] = $val;
	// 		$this->load($arr);
	// 	}
	// }

	public function to_arr(){
		if($this->model != null){
			$arr = [];
			foreach ($this->model as $key => $value) {
				$arr[$key] = $value;
			}
			return $arr;
		}
		return null;
	}

	public function __get($index){
		if(isset($this->{$index}))
			return $this->{$index};
		if($this->model != null)
			return $this->model->{$index};

		return null;
	}

}