<?php

function forInclude($array) {
  foreach( $array as $module ){
    require_once $module;
  }
}

// Require APP class
require_once( APP_DIR . '/router.php' );

forInclude(glob( APP_DIR . '/core/*.class.php'));

// Require composer autoload
if( file_exists(FRAMEWORK_DIR . '/composer/vendor/autoload.php') ){
	require( FRAMEWORK_DIR . '/composer/vendor/autoload.php' );
}

class_alias('\RedBeanPHP\R','\R');

// Define more constants
define('TWIG_CACHE_DIR', FRAMEWORK_DIR . '/cache');

// Require all modules
forInclude(glob( FRAMEWORK_DIR . '/modules/*.module.php' ));

// Require all models
forInclude(glob( APP_DIR . '/models/*.php' ));
